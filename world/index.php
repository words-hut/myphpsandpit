<?php require_once("includes/dbconnection.php"); ?>
<?php include("includes/header.php"); ?>
<?php include("includes/nav.php"); ?>
<?php include("includes/choose.php"); ?>

<main>
<div class="row col-lg-12 col-md-8">

<?php
//PERFORM A QUERY
// SEARCH FORM
if(isset($_GET['country'])){
	$country = strtolower(trim($_GET['country']));
	$country = mysqli_real_escape_string($link, $country);
	} else {
		$country = 'Ireland';
		}
 
 $query = "SELECT * FROM country WHERE name = '{$country}'";
 $result = mysqli_query($link, $query);
 $number_of_rows = mysqli_num_rows($result);
 
 // SOME FACTS ABOUT {COUNTRY}:
 if($result) {
	 echo "<h2 class=\"text-center choose_country\">Some facts about <span class=\"h2span\">{$country}: </h2>";
	;
	 } else {
		 die("Database query failed!");
		 }

// TABLE WITH DATA FROM DATABASE
// Fetch the first row into an array
$country_info = mysqli_fetch_assoc($result);

//Free the results from the query
mysqli_free_result($result);
?> 
  
      <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
          <tbody>
            <tr>
              <td class="column_width">Continent</td>
              <td class="column_width">Region</td>
              <td class="column_width">Population</td>
              <td class="column_width">GNP</td>
              <td class="column_width">Life Expectancy</td>
            </tr>
            <tr>
              <td><?php echo htmlentities($country_info['Continent']); ?></td>
              <td><?php echo htmlentities($country_info['Region']); ?></td>
              <td><?php echo htmlentities($country_info['Population']); ?></td>
              <td><?php echo htmlentities($country_info['GNP']); ?></td>
              <td><?php echo htmlentities($country_info['LifeExpectancy']); ?></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

  </main>

<?php include("includes/footer.php"); ?>
<?php include("includes/closeconnection.php"); ?>