<link href="../css/custom.css" rel="stylesheet" type="text/css">
<link href="../css/bootstrap.css" rel="stylesheet" type="text/css">

<div class="text-center choose">

    <a>
      <h2 class="text-center choose_country" data-toggle="collapse"> 
          <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> 
          Search for the country
      </h2>
    </a>
    
    <form action="index.php" method="get">
        <input type="text" name="country" class="btn-default btn-lg" 
            placeholder="Or tape name of a country here">
        <input type="submit" class="btn-default btn-lg submit_button">
    </form>

</div>
