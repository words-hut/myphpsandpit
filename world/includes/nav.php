<link href="../css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="../css/custom.css" rel="stylesheet" type="text/css">

<nav class="col-lg-4 col-md-4">

<?php
$query2 = "Select name from Country";

$country_name_set = mysqli_query($link, $query2);
if($country_name_set){
	// echo "Query successfull! Number of rows returned: " . mysqli_num_rows($country_name_set);
	} else { die("Database query failed"); }
?>

<div class="nav_panel">
<ul>

<li>
		<?php while($country_names_array = mysqli_fetch_assoc($country_name_set)){ ?>
        <a href="index.php?country=<?php echo rawurlencode($country_names_array['name']); ?>">
        <button class="btn btn-lg button btn-default">
		<?php echo $country_names_array['name']; ?> 
      	</button>
    </a>
    <?php }
	?>

</li>    
</ul>    
</div>
</nav>
