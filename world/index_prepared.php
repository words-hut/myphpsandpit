<?php require_once("includes/dbconnection.php"); ?>
<?php include("includes/header.php"); ?>
<?php include("includes/nav.php"); ?>
<?php include("includes/choose.php"); ?>

<main>
<div class="row col-lg-12 col-md-8">

<?php
  //PERFORM A QUERY
 if(isset($_GET['country'])){
	 $country = $_GET['country'];
	 $country = mysqli_real_escape_string($link, $country);
 } else {
	 $country = 'Ireland';
	 }
	 
 // CREATE THE QUERY
 $query = "SELECT Continent, Region, FORMAT(Population,0), FORMAT(GNP,0), ROUND(LifeExpectancy,1) FROM country WHERE name = ?";
  
 // PREPARE THE STATEMENT
 $prepared_statement = mysqli_prepare($link, $query);

// BIND VARIABLE(S) TO THE PLACEHOLDER(S)/MARKER(S)
mysqli_stmt_bind_param($prepared_statement,"s",$country); // "s" => STRING, "i" => INTEGER


//TRY TO EXECUTE THE STATEMENT
if(mysqli_stmt_execute($prepared_statement)){
	// echo "Prepared Statement executed!";
} else {
	mysqli_stmt_close($prepared_statement);
	die("Prepared Statement Failed!");
	}
	
// BIND VARIABLES TO THE RESULTS OF THE PREPARED STATEMENT
mysqli_stmt_bind_result($prepared_statement, $continent, $region, $population, $gnp, $lifeexpectancy) ;
 
// FETCH THE RESULTS INTO THE BOUND VARIABLES
mysqli_stmt_fetch($prepared_statement);

//Free the results from the query
mysqli_stmt_free_result($prepared_statement);
mysqli_stmt_close($prepared_statement);

// var_dump($continent, $region, $population, $gnp, $lifeexpectancy);
 
// SOME FACTS ABOUT {COUNTRY}:
echo "<h2 class=\"text-center choose_country\">Some facts about <span class=\"h2span\">{$country}: </h2>";

?>   
      <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
          <tbody>
            <tr>
              <td class="column_width">Continent</td>
              <td class="column_width">Region</td>
              <td class="column_width">Population</td>
              <td class="column_width">GNP</td>
              <td class="column_width">Life Expectancy</td>
            </tr>
            <tr>
              <td><?php echo $continent; ?></td>
              <td><?php echo $region; ?></td>
              <td><?php echo $population; ?></td>
              <td><?php echo $gnp; ?></td>
              <td><?php echo $lifeexpectancy; ?></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

  </main>

<?php include("includes/footer.php"); ?>
<?php include("includes/closeconnection.php"); ?>