<?php

$allCountryNames = array(
            "Ireland",
            "China"
);

$allDetails = array(
            array('Name' => 'Ireland', 'Continent' => "Europe", 'Region' => 'region', 'Population' => 'population', 'GNP' => 'gnp', 'LifeExpectancy' => 'lifeExpectancy'),
            array('Name' => 'China', 'Continent' => "Asia", 'Region' => 'region', 'Population' => 'population', 'GNP' => 'gnp', 'LifeExpectancy' => 'lifeExpectancy'),
);
$inputCountry = filter_input(INPUT_GET, 'country', FILTER_SANITIZE_ENCODED);
$selectedCountry = isset($inputCountry) ? $inputCountry : 'Ireland';

foreach ($allDetails as $detail) {
    if (strtolower(trim($detail['Name'])) == strtolower(trim($selectedCountry))) {
        $selectedCountryDetails = $detail;
        break;
    }
}
if (!isset($selectedCountryDetails)) {
    $selectedCountryDetails = array();
}

require_once 'C:\xampp\vendor\twig\twig\lib\Twig\Autoloader.php';
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array(
    'cache' => false
        ));
$template = $twig->loadTemplate('master.html');
echo $template->render(array(
    'countrySelected' => $selectedCountry,
    'details' => $selectedCountryDetails,
    'countryNames' => $allCountryNames));
?>